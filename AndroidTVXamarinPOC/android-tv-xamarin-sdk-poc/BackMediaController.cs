﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace android_tv_xamarin_sdk_poc
{
    public class BackMediaController : MediaController
    {
        private ImageButton mCCBtn;
        private Context mContext;        

        public BackMediaController(Context context) : base(context)
        {
            mContext = context;
        }


        public override void SetAnchorView(View view)
        {
            base.SetAnchorView(view);

            FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(
                    LayoutParams.WrapContent, LayoutParams.WrapContent);
            frameParams.Gravity = GravityFlags.Right | GravityFlags.Top;

            View v = makeCCView();
            AddView(v, frameParams);

        }

        private View makeCCView()
        {
            mCCBtn = new ImageButton(mContext);
           mCCBtn.SetImageResource(Resource.Drawable.arrow_right_3);

            mCCBtn.SetOnClickListener(new OnclickListener());
            return mCCBtn;
        }


        public class OnclickListener : Java.Lang.Object, View.IOnClickListener
        {
            public void OnClick(View v)
            {
                Intent myIntent = new Intent(v.Context, typeof(MainActivity));
                v.Context.StartActivity(myIntent);
            }
        }
    }
}