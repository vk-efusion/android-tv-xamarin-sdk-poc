#How to setup development environment on a pristine machine.

## Dependencies

## Xamarin Android Environment		

1. Install Visual Studio Comunnity

  a. [Download and install Visual Studio Community 2017](https://www.visualstudio.com/xamarin/)  
![a](./images/00VisualStudioToolsForXamarin.png)

	b. Inclide **Mobile Development with .Net** Module  and Wait for the installation to complete:
	
	![01-InstallMobileDevelopmentTools](images/01-InstallMobileDevelopmentTools.png)
	
	![02-InstallVisualStudioComunity](images/02-InstallVisualStudioComunity.png)
	
	c. Launch Visual Studio
	
	![03-lauchVisualStudio](images/03-lauchVisualStudio.png)

#How to deploy the application to an android tv for testing purposes  

## Preparing the Android Device

###Prepare an Android Physical Device

Here are the steps involved with connecting a device to a computer for debugging:

#### Enable Debugging on the Device - By default, it will not be possible to debug applications on a Android device.

Enable Android TV Debugging Mode:

1. Go to “Settings” at the bottom of LeanbackLauncher

![01-enable-debug-device.png](images/01-enable-debug-device.png)

2. Select “About” icon at the top row, most right side

![02-enable-debug-device.png](images/02-enable-debug-device.png)

3. Press “Build” button, located at the bottom for 7 times.

![03-enable-debug-device.png](images/03-enable-debug-device.png)

4. Go back “Home” and Go to “Settings” again, Developer options are now available on the System Preferences row, right most side.

![04-enable-debug-device.png](images/04-enable-debug-device.png)


#### Install USB Drivers - This step is not necessary for OS X computers. Windows computers may require the installation of USB drivers.

It may be necessary to install some extra drivers before a Windows computer will recognize an Android device connected by USB.

1. Run the android.bat application in the **{Android SDK install path}\tools** directory. By default, the Xamarin.Android installer will put the Android SDK in following location on a Windows computer:

**C:\Users\[username]\AppData\Local\Android\android-sdk**

2. Install the Google USB Driver package by starting the Android SDK Manager, and expanding the Extras folder, as can be seen in the follow screenshot:

![usbdriverpackage.png](images/usbdriverpackage.png)

3. Check the **Google USB Driver box**, and click the **Install** button. The driver files are downloaded to the following location:

**[Android SDK install path]\extras\google\usb\_driver**

4. The default path for a Xamarin.Android installation is:

**C:\Users\[username]\AppData\Local\Android\android-sdk\extras\google\usb_driver**

5. Open the System Properties dialog (press Win+Break on the keyboard or locate "Computer" in Start Menu, right-click on it and select "Properties".
6. Click on the "Device Manager" link.

![01-installusbdriver.png](images/01-installusbdriver.png)

7. In the Device Manager locate your Android device. Then right-click on it and select "Update Driver Software".

![02-installusbdriver.png](images/02-installusbdriver.png)

8. Select "Browse my computer for driver software".

![03-installusbdriver.png](images/03-installusbdriver.png)

9. Select "Let me pick from a list of device drivers on my computer".

![04-installusbdriver.png](images/04-installusbdriver.png)

10. Select "Show All Devices".

![05-installusbdriver.png](images/05-installusbdriver.png)

11. Press the "Have Disk" button.

![06-installusbdriver.png](images/06-installusbdriver.png)

12. Enter the path to the Google USB driver. Normally it is located in the following directory: [Android SDK install path]\extras\google\usb_driver

![07-installusbdriver.png](images/07-installusbdriver.png)

13. Select "Android ADB Interface" from the list of device types.

![08-installusbdriver.png](images/08-installusbdriver.png)

14. Confirm the installation of the driver by pressing "Yes".

![09-installusbdriver.png](images/09-installusbdriver.png)

15. Confirm the installation of the driver by pressing "Yes".

![10-installusbdriver.png](images/10-installusbdriver.png)

16. Confirm the installation of the driver by pressing "Yes".

![11-installusbdriver.png](images/11-installusbdriver.png)

#### Connect the Device to the Computer - The final step involves connecting the device to the computer by either USB or WiFi.

Plug device into computer

1. Run **adb devices** to ensure it's connected
2. Run **adb tcpip 5555** (any port number not in use)
3. Run **adb connect [android-tv-ip]:5555**

(**adb** executable can be found under **[Android SDK install path]\platform-tools**)


###Prepare an Android Virtual Device
You will need an Android device to run your Xamarin Android app. You can use an Android Virtual Device which allows you to emulate an Android device on your computer.

#### Installing System Images

You must download and install API level-specific system images that are used by the Android SDK emulator. 
For each Android API level, there are a set of x86 or x64 system images that you will need to download and install for creating virtual devices.

To install the necessary system images, start the Android SDK Manager **(Tools > Android > Android SDK Manager)** 
and scroll to the API level **Android 6.0 - Marshmallow** and enable the check mark next to the following system images:

1) **Android TV Intel x86 Atom System Image**
2) **Google APIs Intel x86 Atom System Image**

![04-select-x86-images-sml](images/04-select-x86-images-sml.png)

After these system images are installed, you can create x86-based Android virtual devices by selecting the appropriate API level and CPU/ABI choices during virtual device configuration

#### Configuring Virtual Devices

Virtual devices are configured via the **Android Emulator Manager** (also referred to as the Android Virtual Device Manager or AVD Manager). 
To launch the Android Emulator Manager from Visual Studio, click the **Android Emulator Manager** icon in the toolbar:

![05-avd-icon-sml](images/05-avd-icon-sml.png)

You can also launch the Android Emulator Manager from the menu bar by selecting Tools > Android > Android Emulator Manager:

![05-avd-manager-menu-item-sml](images/05-avd-manager-menu-item-sml.png)

The **Android Virtual Device (AVD) Manager** dialog displays the list of existing Android virtual devices:

![06-virtual-device-manager-sml](images/06-virtual-device-manager-sml.png)

You can create new virtual device images with different device characteristics and API levels � Go to **Device Definitions**, select **Android TV Image**
and click the **Create ADV** button.

![07-device-definitions-sml](images/07-device-definitions-sml.png)

After OK is clicked, the custom device configuration is displayed in the list of existing Android virtual devices. 
In addition, it is added to the device pull-down menu:

![08-new-avd-sml](images/08-new-avd-sml.png)

[For more information or MAC instructions click here.](https://docs.microsoft.com/en-us/xamarin/android/get-started/installation/android-emulator/google-emulator-manager?tabs=vswin)

#How to build the application  

1. Clone the Repository - **git clone https://mairadanielaferrari@bitbucket.org/mairadanielaferrari/android-tv-xamarin-sdk-poc.git**
2. cd AndroidTVXamarinPOC
3. Open Solution (AndroidTVXamarinPOC.sln) using Visual Studio
4. Select Android TV Device and Execute Visual Studio Build.

#How to publish the application to Google Play  

[Instructions to Configura and Publish to Google Play](https://docs.microsoft.com/en-us/xamarin/android/deploy-test/publishing/publishing-to-google-play/?tabs=vswin)

